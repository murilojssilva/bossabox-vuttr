import React, { useState } from "react";
import "./style.css";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import * as Yup from "yup";

import { signInRequest } from "../../store/modules/auth/actions";

const schema = Yup.object().shape({
  email: Yup.string()
    .email("Insira um e-mail válido.")
    .required("O e-mail é obrigatório."),
  password: Yup.string().required("A senha é obrigatória"),
});

export default function Login() {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.auth.loading);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(e) {
    dispatch(signInRequest(email, password));
    e.preventDefault();
  }

  return (
    <div className="login">
      <h1>Login</h1>
      <form onSubmit={handleSubmit} schema={schema} className="login-form">
        <input
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          name="email"
          placeholder="Seu e-mail"
          type="email"
        />
        <input
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          name="password"
          placeholder="Sua senha"
          type="password"
        />
        <button type="submit">{loading ? "Carregando..." : "Acessar"}</button>
        <Link to="/register">Criar cadastro</Link>
      </form>
    </div>
  );
}
