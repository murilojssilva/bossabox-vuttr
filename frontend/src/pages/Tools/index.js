import React, { useEffect } from "react";
import "./style.css";
import { useDispatch } from "react-redux";
import api from "../../services/api";

import { signOut } from "../../store/modules/auth/actions";

export default function Tools() {
  const dispatch = useDispatch();
  const [tools, setTools] = React.useState([]);

  useEffect(function effectFunction() {
    async function fetchTools() {
      const response = await api.get("/tools");
      setTools(response.data);
    }
    fetchTools();
  }, []);

  function handleSignOut() {
    dispatch(signOut());
  }

  return (
    <div className="tools">
      <button onClick={handleSignOut}>Sair da aplicação</button>
      <h3>Mostrar as ferramentas</h3>
      <ul>
        {tools.map((tool, index) => (
          <div key={index}>
            <li key={tool.title}>Nome: {tool.title}</li>
            <li key={tool.link}>
              Links: <a href="{tool.link}">{tool.link}</a>
            </li>
            <li key={tool.description}>Descrição: {tool.description}</li>
            <li key={tool.tags}>
              {tool.tags.map((tag, index) => (
                <a key={index} href={`/tags/${tag}`}>
                  {tag},{" "}
                </a>
              ))}
            </li>
          </div>
        ))}
      </ul>
    </div>
  );
}
