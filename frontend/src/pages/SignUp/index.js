import React, { useState } from "react";
import { useDispatch } from "react-redux";
import "./style.css";
import { Link } from "react-router-dom";
import * as Yup from "yup";

import { signUpRequest } from "../../store/modules/auth/actions";

const schema = Yup.object().shape({
  name: Yup.string().required("O nome é obrigatório."),
  email: Yup.string()
    .email("Insira um e-mail válido")
    .required("O e-mail é obrigatório."),
  password: Yup.string()
    .min(6, "A senha deve possuir, no mínimo, 6 dígitos.")
    .required("A senha é obrigatória"),
});

export default function SignUp() {
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  function handleSubmit(e) {
    dispatch(signUpRequest(name, email, password));
    e.preventDefault();
  }

  return (
    <div className="register">
      <h1>Cadastro</h1>
      <form schema={schema} onSubmit={handleSubmit} className="register-form">
        <input
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Seu nome completo"
          type="text"
        />
        <input
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Seu e-mail"
          type="email"
        />
        <input
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Sua senha"
          type="password"
        />
        <button>Criar conta</button>
        <Link to="/">Já possuo cadastro</Link>
      </form>
    </div>
  );
}
