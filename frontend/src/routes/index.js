import React from "react";
import Login from "../pages/Login";
import SignUp from "../pages/SignUp";
import Tools from "../pages/Tools";

import { Switch } from "react-router-dom";

import Route from "./Route";

export default function AuthRoutes() {
  return (
    <Switch>
      <Route path="/tools" exact component={Tools} isPrivate></Route>
      <Route path="/" exact component={Login}></Route>
      <Route path="/register" component={SignUp}></Route>
    </Switch>
  );
}
